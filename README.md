Agregue contenido ...

Command line instructions
You can also upload existing files from your computer using the instructions below.


Git global setup
git config --global user.name "Clara Sotello"
git config --global user.email "cigsotello@gmail.com"

Create a new repository
git clone https://gitlab.com/b06g01optimus/b06g01seguimiento-mediciones-control-de-calidad.git
cd b06g01seguimiento-mediciones-control-de-calidad
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main

Push an existing folder
cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/b06g01optimus/b06g01seguimiento-mediciones-control-de-calidad.git
git add .
git commit -m "Initial commit"
git push -u origin main

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/b06g01optimus/b06g01seguimiento-mediciones-control-de-calidad.git
git push -u origin --all
git push -u origin --tags
